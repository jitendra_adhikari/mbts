<?php

// configure your app for the production environment

// Setup twig
$app['twig.path'] = [__DIR__.'/../templates'];
$app['twig.options'] = ['cache' => __DIR__.'/../var/cache/twig'];

// Setup DB params
$app['db.options'] = [
    'driver' => 'pdo_sqlite',
    'path' => __DIR__.'/../var/app.db',
];

// Setup migration
$app['db.migrations'] = [
    'directory' => __DIR__.'/../src/Migration',
    'name' => 'Migrations',
    'namespace' => 'Ahc\Migration',
    'table_name' => 'migrations',
];

// Setup twitter credentials
$app['twitter.credentials'] = [
    'oauth_access_token' => '962030066-H3ue1gp34LxQHFK7JQh7RiOqlgBhmE0cAScno5XT',
    'oauth_access_token_secret' => '9OoYHD2okfwow2ul8Jy0bOFDMbM0d61iIM7idXCUC5VnS',
    'consumer_key' => '7juzoXJpb84lSNm1DSYbVDn3x',
    'consumer_secret' => 'zwI25TNYG4icXCpB3cKrgi8hoc74GamkalOe1QhfZl6UdJOrh8',
];

// Setup twitter options
$app['twitter.request_method'] = 'GET';
$app['twitter.results_per_page'] = 25;
$app['twitter.tweet_search_endpoint'] = 'https://api.twitter.com/1.1/search/tweets.json';

// Setup session options
$app['session.storage.options'] = [
    'name' => 'mbts_prod',
    'cookie_lifetime' => 2592000,
];
