<?php

use Silex\Provider\MonologServiceProvider;
use Silex\Provider\WebProfilerServiceProvider;
use Silex\Provider\HttpFragmentServiceProvider;

// include the prod configuration
require __DIR__.'/prod.php';

// enable the debug mode
$app['debug'] = true;

$app->register(new MonologServiceProvider(), array(
    'monolog.logfile' => __DIR__.'/../var/logs/app_dev.log',
));

// $app->register(new WebProfilerServiceProvider(), array(
//     'profiler.cache_dir' => __DIR__.'/../var/cache/profiler',
// ));

$app->register(new HttpFragmentServiceProvider());

/* Configs for dev mode that overrides prod configs */
// Setup session options
$app['session.storage.options'] = [
    'name' => 'mbts_dev',
    'cookie_lifetime' => 86400,
];
