<?php

use Silex\Provider\MonologServiceProvider;

// First, include the `prod` configuration
require __DIR__.'/prod.php';

// Then override for `test` env:

// Setup DB params
$app['db.options'] = [
    'driver' => 'pdo_sqlite',
    'path' => __DIR__.'/../var/app_test.db',
];

// Not using sessions
$app['session.test'] = true;
$app['session.storage.handler'] = null;

// Minimize results per page
$app['twitter.results_per_page'] = 2;

// Random cookie ID
$app['session.cookie_id'] = md5(date('Y-m-d H'));

// Logger
$app->register(new MonologServiceProvider(), array(
    'monolog.logfile' => __DIR__.'/../var/logs/app_test.log',
));
