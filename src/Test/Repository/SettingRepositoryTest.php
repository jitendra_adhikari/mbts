<?php

namespace Ahc\Test\Repository;

use Ahc\Test\RepositoryTestCase;

class SettingRepositoryTest extends RepositoryTestCase
{
    public function testSetGet()
    {
        $repo = $this->app['settings_repo'];

        $actual = 'The actual value';
        $bool = $repo->set('some_key', $actual);
        $this->assertTrue(is_bool($bool));
        $this->assertEquals($repo->get('some_key'), $actual);

        $actual = 'The actual value updated';
        $bool = $repo->set('some_key', $actual);
        $this->assertTrue(is_bool($bool));
        $this->assertEquals($repo->get('some_key'), $actual);
    }
}
