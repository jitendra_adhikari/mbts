<?php

namespace Ahc\Test\Repository;

use Ahc\Test\RepositoryTestCase;
use Silex\Application;

class HistoryRepositoryTest extends RepositoryTestCase
{
    public function init(Application $app)
    {
        $app['session.cookie_id'] = md5(date('Y-m-d H'));
    }

    public function testSaveAndRetrieve()
    {
        $repo = $this->app['histories_repo'];
        $location = 'Pokhara';
        $identifier = $this->app['session.cookie_id'];

        $history = $repo->save($location, $identifier);
        $this->assertArrayHasKey('id', $history);
        $this->assertArrayHasKey('location', $history);

        $meta = $this->app['json.meta'];
        $this->app['json.meta'] = []; // Clear meta
        $this->assertArrayHasKey('is_new', $meta);

        // Retrieve
        $histories = $repo->history($identifier);
        $this->assertEquals('array', gettype($histories));
        $this->assertContains(
            $history,
            $histories
        );

        $meta = $this->app['json.meta'];
        $this->app['json.meta'] = []; // Clear meta
        $this->assertArrayHasKey('count', $meta);
        $this->assertEquals(count($histories), intval($meta['count']));
    }
}
