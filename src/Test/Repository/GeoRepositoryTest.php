<?php

namespace Ahc\Test\Repository;

use Ahc\Test\RepositoryTestCase;
use Ahc\Provider\GeolocationServiceProvider;
use Silex\Application;

class GeoRepositoryTest extends RepositoryTestCase
{
    public function init(Application $app)
    {
        $app->register(new GeolocationServiceProvider());
    }

    public function testLookup()
    {
        $repo = $this->app['geo_repo'];
        $coords = $repo->lookup('Pokhara');

        $this->assertArrayHasKey('latitude', $coords);
        $this->assertArrayHasKey('longitude', $coords);
        $this->assertTrue(is_numeric($coords['latitude']));
        $this->assertTrue(is_numeric($coords['longitude']));

        $this->assertGreaterThan(28, $coords['latitude']);
        $this->assertGreaterThan(83, $coords['longitude']);
    }
}
