<?php

namespace Ahc\Test\Repository;

use Ahc\Test\RepositoryTestCase;
use Ahc\Provider\TwitterServiceProvider;
use Silex\Application;

class TweetRepositoryTest extends RepositoryTestCase
{
    public function init(Application $app)
    {
        $app->register(new TwitterServiceProvider(), [
            'twitter.request_method' => 'GET',
            'twitter.results_per_page' => 2,
            'twitter.tweet_search_endpoint' => 'https://api.twitter.com/1.1/search/tweets.json',
        ]);
    }

    public function testLookupAndReload()
    {
        $repo = $this->app['tweets_repo'];
        $tweets = $repo->lookup('Pokhara', '28.237987,83.995587');
        $meta = $this->app['json.meta'];
        $this->app['json.meta'] = []; // Clear

        $this->assertEquals('array', gettype($tweets));
        $this->assertArrayHasKey('is_cache', $meta);
        $this->assertArrayHasKey('rsrc_id', $meta);

        $moreTweets = [];
        if (isset($meta['max_id'])) {
            $moreTweets = $repo->reload($meta['rsrc_id'], $meta['max_id']);
            $meta = $this->app['json.meta'];
            $this->app['json.meta'] = []; // Clear

            $this->assertEquals('array', gettype($moreTweets));
            $this->assertArrayHasKey('is_cache', $meta);
            $this->assertArrayHasKey('rsrc_id', $meta);
        }

        // Fetch the tweets again
        $cachedTweets = $repo->lookup('Pokhara', '28.237987,83.995587');
        $this->assertEquals('array', gettype($cachedTweets));
        // It should use cache
        $this->assertTrue($meta['is_cache']);
        // The cached tweets has to be the ones we fetched earlier
        $this->assertEquals($cachedTweets, array_merge($tweets, $moreTweets));
    }
}
