<?php

namespace Ahc\Test;

use Silex\WebTestCase as ParentTestCase;

class WebTestCase extends ParentTestCase
{
    /**
     * {@inheritdoc}
     */
    public function createApplication()
    {
        return require 'app.php';
    }

    public function setUp()
    {
        parent::setUp();

        $this->init();

        $this->app['db']->executeQuery('CREATE TABLE IF NOT EXISTS `settings` (`name` VARCHAR(255) NOT NULL, `value` TEXT NOT NULL, PRIMARY KEY(`name`))');
        $this->app['db']->executeQuery('CREATE TABLE IF NOT EXISTS `tweets` (`id` INTEGER PRIMARY KEY AUTOINCREMENT, `location` VARCHAR(255) NOT NULL, `latitude` DECIMAL(10, 7), `longitude` DECIMAL(10, 7), `tweets` TEXT NOT NULL, `searched_on` INTEGER NOT NULL)');
        $this->app['db']->executeQuery('CREATE TABLE IF NOT EXISTS `histories` (`id` INTEGER PRIMARY KEY AUTOINCREMENT, `cookie` VARCHAR(255) NOT NULL, `location` TEXT NOT NULL)');
    }

    /**
     * A proxy for setUp() to be used by subclasses
     */
    public function init()
    {
    }

    /**
     * Creates symfony browserkit client, makes request
     * to the given resource and gives back response.
     *
     * @param  string     The resource with http method. eg: GET /foo/bar
     * @param  array      The parameters to be sent with request to the resource
     *
     * @return Response An instance of Symfony\Component\HttpFoundation\Response
     */
    protected function browse($resource, array $parameters = [])
    {
        list($method, $uri) = explode(' ', $resource, 2);

        $client = $this->createClient();
        $client->request($method, $uri, $parameters);

        return $client->getResponse();
    }
}
