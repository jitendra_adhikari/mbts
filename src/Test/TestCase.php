<?php

namespace Ahc\Test;

use Silex\Application;

class TestCase extends \PHPUnit_Framework_TestCase
{
    protected $app;
    protected $registry = [];

    public function setUp()
    {
        parent::setUp();

        $this->init($this->app());

        if (isset($this->app['db'])) {
            $this->app['db']->executeQuery('CREATE TABLE IF NOT EXISTS `settings` (`name` VARCHAR(255) NOT NULL, `value` TEXT NOT NULL, PRIMARY KEY(`name`))');
            $this->app['db']->executeQuery('CREATE TABLE IF NOT EXISTS `tweets` (`id` INTEGER PRIMARY KEY AUTOINCREMENT, `location` VARCHAR(255) NOT NULL, `latitude` DECIMAL(10, 7), `longitude` DECIMAL(10, 7), `tweets` TEXT NOT NULL, `searched_on` INTEGER NOT NULL)');
            $this->app['db']->executeQuery('CREATE TABLE IF NOT EXISTS `histories` (`id` INTEGER PRIMARY KEY AUTOINCREMENT, `cookie` VARCHAR(255) NOT NULL, `location` TEXT NOT NULL)');
        }
    }

    /**
     * A proxy for setUp() to be used by subclasses
     */
    public function init(Application $app)
    {
    }

    protected function app(array $deps = [])
    {
        if (!$this->app) {
            $this->app = new Application();

            // The service providers
            foreach ($this->registry as $service => $options) {
                $this->app->register(new $service(), $options);
            }
        }

        // The dependencies or configs
        foreach ($deps as $key => $value) {
            $this->app[$key] = $value;
        }

        return $this->app;
    }
}
