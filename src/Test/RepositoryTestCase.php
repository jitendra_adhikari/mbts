<?php

namespace Ahc\Test;

class RepositoryTestCase extends TestCase
{
    protected $registry = [
        'Silex\\Provider\\DoctrineServiceProvider' => [
            'db.options' => [
                'driver' => 'pdo_sqlite',
                'path' => __DIR__.'/../../var/app_test.db',
            ],
        ],
        'Ahc\\Provider\\CommonServiceProvider' => [],
    ];
}
