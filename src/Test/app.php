<?php

ini_set('date.timezone', 'UTC');

$app = new Ahc\Application();
$app->setup();

require __DIR__.'/../../config/test.php';
require __DIR__.'/../../src/controllers.php';

$console = require __DIR__.'/../../src/console.php';
$app->cliSetup($console);

return $app;
