<?php

namespace Ahc\Test\Provider;

use Ahc\Test\TestCase;

class GeolocationServiceProviderTest extends TestCase
{
    protected $registry = [
        'Ahc\\Provider\\GeolocationServiceProvider' => [],
    ];

    public function testHandlerAndHelper()
    {
        $handler = $this->app['geo.handler'];
        $this->assertInstanceOf('JeroenDesloovere\Geolocation\Geolocation', $handler);

        $helper = $this->app['geo.get_coords'];
        $expected = [
            'latitude' => 28.237987,
            'longitude' => 83.9955879,
        ];
        $this->assertEquals($expected, $helper('Pokhara'));
    }
}
