<?php

namespace Ahc\Test\Provider;

use Ahc\Test\TestCase;

class TwitterServiceProviderTest extends TestCase
{
    protected $registry = [
        'Ahc\\Provider\\TwitterServiceProvider' => [
            'twitter.request_method' => 'GET',
            'twitter.results_per_page' => 2,
            'twitter.tweet_search_endpoint' => 'https://api.twitter.com/1.1/search/tweets.json',
        ],
    ];

    public function testHandlerAndHelper()
    {
        $this->assertArrayHasKey('oauth_access_token', $this->app['twitter.credentials']);
        $this->assertArrayHasKey('consumer_secret', $this->app['twitter.credentials']);
        $this->assertTrue(isset($this->app['twitter.results_per_page']));

        $handler = $this->app['twitter.handler'];
        $this->assertInstanceOf('TwitterAPIExchange', $handler);
        $count = $this->app['twitter.results_per_page'];
        $helper = $this->app['twitter.search_tweets'];
        $payload = $helper('Pokhara', '28.237987,83.9955879,50km', $count);
        $payload = json_decode($payload, true);

        $this->assertArrayHasKey('statuses', $payload);
        $this->assertArrayHasKey('search_metadata', $payload);

        $this->assertLessThanOrEqual($count, count($payload['statuses']));
    }
}
