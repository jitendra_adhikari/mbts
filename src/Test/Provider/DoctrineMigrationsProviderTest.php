<?php

namespace Ahc\Test\Provider;

use Ahc\Test\TestCase;
use Ahc\Provider\DoctrineMigrationsProvider;
use Silex\Application;
use Symfony\Component\Console\Application as Console;

class DoctrineMigrationsProviderTest extends TestCase
{
    protected $registry = [
        'Silex\\Provider\\DoctrineServiceProvider' => [
            'db.options' => [
                'driver' => 'pdo_sqlite',
                'memory' => true,
            ],
        ],
    ];

    public function init(Application $app)
    {
        $app['console'] = new Console('CLI Application', 'n/a');

        $app->register(new DoctrineMigrationsProvider($app['console']));
    }

    public function testMigrationHelpersAndCommands()
    {
        $this->assertTrue($this->app['console']->getHelperSet()->has('db'));
        $this->assertTrue($this->app['console']->getHelperSet()->has('dialog'));

        $this->assertTrue($this->app['console']->has('migrations:diff'));
        $this->assertTrue($this->app['console']->has('migrations:status'));
        $this->assertTrue($this->app['console']->has('migrations:version'));
        $this->assertTrue($this->app['console']->has('migrations:execute'));
        $this->assertTrue($this->app['console']->has('migrations:generate'));
        $this->assertTrue($this->app['console']->has('migrations:migrate'));
    }
}
