<?php

namespace Ahc\Test\Provider;

use Ahc\Test\TestCase;
use Silex\Application;

class CommonServiceProviderTest extends TestCase
{
    protected $registry = [
        'Silex\\Provider\\DoctrineServiceProvider' => [
            'db.options' => [
                'driver' => 'pdo_sqlite',
                'path' => __DIR__.'/../../../var/app_test.db',
            ],
        ],
        'Ahc\\Provider\\CommonServiceProvider' => [],
    ];

    public function init(Application $app)
    {
        $app['settings_repo'] = $this
            ->getMockBuilder('Ahc\\Repository\\SettingRepository')
            ->disableOriginalConstructor()
            ->getMock()
        ;
    }

    public function testUtilsHelpers()
    {
        $actual = $this->app['utils.controller']('ns.ctrl');
        $expected = 'Ahc\Controller\Ns\CtrlController::indexAction';
        $this->assertEquals($expected, $actual);

        $actual = $this->app['utils.controller']('ctrl::get');
        $expected = 'Ahc\Controller\CtrlController::getAction';
        $this->assertEquals($expected, $actual);

        $this->app['json.add_meta'](['test_key' => 'Test Value']);
        $this->assertTrue(isset($this->app['json.meta']['test_key']));

        $this->app['json.add_meta'](['test_key' => 'Test Value Updated']);
        $actual = $this->app['json.meta'];
        $this->assertTrue(is_array($actual));
        $this->assertEquals('Test Value Updated', $actual['test_key']);
    }

    public function testRepos()
    {
        $this->assertInstanceOf('Ahc\\Repository\\TweetRepository', $this->app['tweets_repo']);
        $this->assertInstanceOf('Ahc\\Repository\\GeoRepository', $this->app['geo_repo']);
        $this->assertInstanceOf('Ahc\\Repository\\HistoryRepository', $this->app['histories_repo']);
        $this->assertInstanceOf('Ahc\\Repository\\SettingRepository', $this->app['settings_repo']);
    }
}
