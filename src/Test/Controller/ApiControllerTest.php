<?php

namespace Ahc\Test\Controller;

use Ahc\Test\WebTestCase;

class ApiControllerTest extends WebTestCase
{
    /**
     * @dataProvider echoParameters
     */
    public function testEchoAction()
    {
        list($how, $what) = func_get_args();

        $response = $this->browse('GET /api/echo', compact('what', 'how'));
        $this->assertEquals(200, $response->getStatusCode());

        $content = $response->getContent();
        if ($how === 'text') {
            $this->assertEquals($what, $content);
        } else {
            $this->assertJsonStringEqualsJsonString(
                json_encode(compact('what')),
                $content
            );
        }
    }

    public function echoParameters()
    {
        return [
            ['text', 'This is echoed as text'],
            ['json', 'This is echoed as json'],
        ];
    }

    /**
     * @dataProvider geoSearchParameters
     */
    public function testGeoSearchAction()
    {
        list($type, $location) = func_get_args();
        $response = $this->browse('GET /api/geo-lookup', compact('location'));

        $contentType = $response->headers->get('Content-Type');
        $this->assertEquals('application/json', $contentType);

        $payload = json_decode($response->getContent(), true);

        $this->assertTrue(is_array($payload));
        $this->assertTrue(is_bool($payload['status']));

        if ('valid' === $type) {
            $data = $payload['data'];

            $this->assertTrue(array_key_exists('latitude', $data));
            $this->assertTrue(array_key_exists('longitude', $data));

            $this->assertGreaterThan(0, $data['latitude']);
            $this->assertGreaterThan(0, $data['longitude']);

            return [$location, $data];
        }

        $this->assertTrue(isset($payload['message']));
        $this->assertTrue(is_string($payload['message']));

        return [$location, []];
    }

    public function geoSearchParameters()
    {
        return [
            ['valid', 'Pokhara'],
        ];
    }

    /**
     * @depends      testGeoSearchAction
     * @dataProvider tweetSearchParameters
     */
    public function testTweetSearchAction()
    {
        list($location, $lat, $long) = func_get_args();

        $latlong = implode(',', [$lat, $long]);
        $this->doJsonApiTest(
            'POST /api/tweets-lookup',
            compact('location', 'latlong')
        );
    }

    public function tweetSearchParameters()
    {
        return [
            ['Pokhara', 28.237987, 83.99558],
        ];
    }

    /**
     * @depends      testTweetSearchAction
     * @dataProvider tweetUpdateParameters
     */
    public function testTweetUpdateAction()
    {
        list($rsrc_id, $max_id) = func_get_args();
        $this->doJsonApiTest(
            'POST /api/tweets-update',
            compact('rsrc_id', 'max_id')
        );
    }

    public function tweetUpdateParameters()
    {
        return [
            [1, 1],
        ];
    }

    public function testHistorySaveAction()
    {
        $location = 'Pokhara';
        $this->doJsonApiTest(
            'POST /api/save-history',
            compact('location')
        );
    }

    public function testHistoryGetAction()
    {
        $this->doJsonApiTest('GET /api/get-history');
    }

    protected function doJsonApiTest($resource, array $params = [], callable $successFn = null, callable $failureFn = null)
    {
        $response = $this->browse($resource, $params);

        $contentType = $response->headers->get('Content-Type');
        $this->assertEquals('application/json', $contentType);

        $payload = json_decode($response->getContent(), true);

        $this->assertTrue(is_array($payload));
        $this->assertTrue(is_bool($payload['status']));

        if ($payload['status']) {
            $this->assertNotEmpty($payload['data']);
            $this->assertNotEmpty($payload['meta']);
            if ($successFn) {
                call_user_func(
                    $successFn,
                    $payload['data'], $payload['meta']
                );
            }

            return $response;
        }

        $this->assertTrue(isset($payload['message']));
        $this->assertTrue(is_string($payload['message']));

        if ($failureFn) {
            call_user_func(
                $failureFn,
                $payload['message']
            );
        }

        return $response;
    }
}
