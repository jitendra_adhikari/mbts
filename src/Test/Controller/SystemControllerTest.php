<?php

namespace Ahc\Test\Controller;

use Ahc\Test\WebTestCase;

class SystemControllerTest extends WebTestCase
{
    /**
     * @dataProvider getSystemUris
     */
    public function testEndpoints()
    {
        list($uri, $content) = func_get_args();

        $response = $this->browse('GET '.$uri);
        $this->assertEquals(200, $response->getStatusCode());

        $this->assertContains(
            $content,
            $response->getContent()
        );
    }

    public function getSystemUris()
    {
        return [
            ['/system/purge', 'Not implemented'],
            ['/system/migrate', 'Not implemented'],
            ['/system/settings', 'Settings - Map Based Tweet Search App'],
        ];
    }
}
