<?php

namespace Ahc\Test\Controller;

use Ahc\Test\WebTestCase;

class HomeControllerTest extends WebTestCase
{
    public function testIndexAction()
    {
        $response = $this->browse('GET /');
        $this->assertEquals(200, $response->getStatusCode());

        $this->assertContains(
            '<title>Home - Map Based Tweet Search</title>',
            $response->getContent()
        );
    }

    public function testCoreJsAction()
    {
        $response = $this->browse('GET /core.js');
        $this->assertEquals(200, $response->getStatusCode());

        $this->assertContains(
            'var Core = Core || {};',
            $response->getContent()
        );

        $contentType = $response->headers->get('Content-Type');
        $this->assertContains('text/javascript', $contentType);
    }
}
