<?php

// Instantiate app for normal usage

use Ahc\Application;

$app = new Application();
$app->setup();

return $app;
