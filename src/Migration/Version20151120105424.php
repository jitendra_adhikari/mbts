<?php

namespace Ahc\Migration;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20151120105424 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE `histories1` (`id` INTEGER PRIMARY KEY AUTOINCREMENT, `cookie` VARCHAR(255) NOT NULL, `location` VARCHAR(255) NOT NULL COLLATE NOCASE)');
        $this->addSql('INSERT INTO `histories1` SELECT * FROM `histories`');
        $this->addSql('DROP TABLE `histories`');
        $this->addSql('ALTER TABLE `histories1` RENAME TO `histories`');

        $this->addSql('CREATE TABLE `tweets1` (`id` INTEGER PRIMARY KEY AUTOINCREMENT, `location` VARCHAR(255) NOT NULL COLLATE NOCASE, `latitude` DECIMAL(10, 7), `longitude` DECIMAL(10, 7), `tweets` TEXT NOT NULL, `searched_on` INTEGER NOT NULL)');
        $this->addSql('INSERT INTO `tweets1` SELECT * FROM `tweets`');
        $this->addSql('DROP TABLE `tweets`');
        $this->addSql('ALTER TABLE `tweets1` RENAME TO `tweets`');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
