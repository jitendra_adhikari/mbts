<?php

namespace Ahc\Migration;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20151108123822 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE `tweets` (`id` INTEGER PRIMARY KEY AUTOINCREMENT, `location` VARCHAR(255) NOT NULL, `latitude` DECIMAL(10, 7), `longitude` DECIMAL(10, 7), `tweets` TEXT NOT NULL, `searched_on` INTEGER NOT NULL)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
         $this->addSql('DROP TABLE `tweets`');
    }
}
