<?php

namespace Ahc;

use Silex\Application as SilexApp;
use Silex\Provider\TwigServiceProvider;
use Silex\Provider\SessionServiceProvider;
use Silex\Provider\DoctrineServiceProvider;
use Silex\Provider\ValidatorServiceProvider;
use Silex\Provider\UrlGeneratorServiceProvider;
use Silex\Provider\ServiceControllerServiceProvider;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Console\Application as Console;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Ahc\Provider\CommonServiceProvider;
use Ahc\Provider\DoctrineMigrationsProvider;
use Ahc\Provider\TwitterServiceProvider;
use Ahc\Provider\GeolocationServiceProvider;

/**
 * Just a placeholder to extend Silex\Application if need be.
 */
class Application extends SilexApp
{
    /**
     * Setup app by registering or configuring services or providers.
     */
    public function setup()
    {
        $this->registerProviders();
        $this->registerMiddlewares();
    }

    /**
     * Setup app to run in cli mode by registering or configuring services or providers.
     */
    public function cliSetup(Console $console)
    {
        $this['console'] = $console;

        $this->registerCliProviders($console);
    }

    /**
     * Register the service providers for the application.
     */
    protected function registerProviders()
    {
        $this->register(new UrlGeneratorServiceProvider());
        $this->register(new ValidatorServiceProvider());
        $this->register(new ServiceControllerServiceProvider());
        $this->register(new TwigServiceProvider());
        $this['twig'] = $this->share($this->extend('twig', function ($twig, $this) {
            // add custom globals, filters, tags, ...

            return $twig;
        }));

        $this->register(new DoctrineServiceProvider());
        $this->register(new CommonServiceProvider());
        $this->register(new TwitterServiceProvider());
        $this->register(new GeolocationServiceProvider());
        $this->register(new SessionServiceProvider());
    }

    /**
     * Register the service providers for the cli application.
     */
    protected function registerCliProviders(Console $console)
    {
        $this->register(new DoctrineMigrationsProvider($console));
    }

    /**
     * Registers the middlewares for application.
     */
    protected function registerMiddlewares()
    {
        // Forces session cookie creation and
        // Injects cookie_id into Application instance
        $this->before(function (Request $request, SilexApp $app) {
            // Make sure session exists
            if (isset($app['session']) and !$app['session.test']) {
                // Force session cookie creation
                $app['session']->get('sess_id');
                $app['session.cookie_id'] = $request->cookies->get(
                    $app['session.storage.options']['name']
                );
            }
        });

        // For JsonResponse, append 'meta' if possible
        $this->after(function (Request $request, Response $response) {
            if ($response instanceof JsonResponse and isset($this['json.meta'])) {
                $content = json_decode($response->getContent());
                if (is_object($content) and !isset($content->meta)) {
                    $content->meta = $this['json.meta'];
                    $response->setData($content);
                }
            }
        });
    }
}
