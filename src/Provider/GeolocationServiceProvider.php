<?php

namespace Ahc\Provider;

use Silex\Application;
use Silex\ServiceProviderInterface;
use JeroenDesloovere\Geolocation\Geolocation;

/**
 * Geolocation service provider wrapped around Geolocation.
 *
 * @author Jitendra Adhikari <jiten.adhikary@gmail.com>
 */
class GeolocationServiceProvider implements ServiceProviderInterface
{
    /**
     * {@inheritdoc}
     */
    public function boot(Application $app)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function register(Application $app)
    {
        // One time initiate gelocation
        $app['geo.handler'] = $app->share(function () {
            return new Geolocation();
        });

        // The geoip coordinates lookup helper
        $app['geo.get_coords'] = $app->protect(
            function ($location) use ($app) {
                if (is_array($location)) {
                    $location = implode(' ', $location);
                }

                return $app['geo.handler']
                    ->getCoordinates($location)
                ;
            }
        );
    }
}
