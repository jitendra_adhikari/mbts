<?php

namespace Ahc\Provider;

use Silex\Application;
use Silex\ServiceProviderInterface;
use Symfony\Component\Console\Helper\HelperSet;
use Symfony\Component\Console\Helper\DialogHelper;
use Symfony\Component\Console\Application as Console;
use Doctrine\DBAL\Tools\Console\Helper\ConnectionHelper;
use Doctrine\DBAL\Migrations\Configuration\Configuration;

/**
 * Class DoctrineMigrationsProvider.
 */
class DoctrineMigrationsProvider implements ServiceProviderInterface
{
    /**
     * The console application.
     *
     * @var Console
     */
    protected $console;

    /**
     * Creates a new doctrine migrations provider.
     *
     * @param Console $console
     */
    public function __construct(Console $console)
    {
        $this->console = $console;
    }

    /**
     * {@inheritdoc}
     */
    public function register(Application $app)
    {
        $this->console->setHelperSet(
            new HelperSet([
                'db' => new ConnectionHelper($app['db']),
                'dialog' => new DialogHelper(),
            ])
        );

        $commands = array(
            'Doctrine\DBAL\Migrations\Tools\Console\Command\DiffCommand',
            'Doctrine\DBAL\Migrations\Tools\Console\Command\ExecuteCommand',
            'Doctrine\DBAL\Migrations\Tools\Console\Command\GenerateCommand',
            'Doctrine\DBAL\Migrations\Tools\Console\Command\MigrateCommand',
            'Doctrine\DBAL\Migrations\Tools\Console\Command\StatusCommand',
            'Doctrine\DBAL\Migrations\Tools\Console\Command\VersionCommand',
        );

        $this->setDefaults($app);
        $conf = new Configuration($app['db']);

        $conf->setName($app['db.migrations']['name']);
        $conf->setMigrationsDirectory($app['db.migrations']['directory']);
        $conf->setMigrationsNamespace($app['db.migrations']['namespace']);
        $conf->setMigrationsTableName($app['db.migrations']['table_name']);
        $conf->registerMigrationsFromDirectory($app['db.migrations']['directory']);

        foreach ($commands as $name) {
            $command = new $name();
            $command->setMigrationConfiguration($conf);
            $this->console->add($command);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function boot(Application $app)
    {
    }

    /**
     * Set default parameters required by migration if only
     * they are not already injected to DI wrapper (the app)
     *
     * @param Application $app
     */
    protected function setDefaults(Application $app)
    {
        $defaults = [
            'directory' => __DIR__.'/../Migration',
            'name' => 'Migrations',
            'namespace' => 'Ahc\Migration',
            'table_name' => 'migrations',
        ];
        if (!isset($app['db.migrations'])) {
            $app['db.migrations'] = $defaults;
        }
    }
}
