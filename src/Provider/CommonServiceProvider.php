<?php

namespace Ahc\Provider;

use Silex\Application;
use Silex\ServiceProviderInterface;
use Ahc\Repository\GeoRepository;
use Ahc\Repository\TweetRepository;
use Ahc\Repository\HistoryRepository;
use Ahc\Repository\SettingRepository;

/**
 * Common utils/tools often used as helpers or shortcuts.
 *
 * @author Jitendra Adhikari <jiten.adhikary@gmail.com>
 */
class CommonServiceProvider implements ServiceProviderInterface
{
    /**
     * {@inheritdoc}
     */
    public function boot(Application $app)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function register(Application $app)
    {
        // The wrapper for controller shorthand representation
        // Usage:  $app['utils.controller']('blog.comment::post')
        //        'Ahc\Controller\Blog\CommentController::postAction'
        $app['utils.controller'] = $app->protect(
            /**
             * Returns fully qualified classname with method for given segment.
             *
             * @param string The segment where class and method are delimited by '::', like so blog::index
             */
            function ($segment) {
                // Default method 'index'
                if (strpos($segment, '::') === false) {
                    $segment .= '::index';
                }

                list($class, $method) = explode('::', $segment);

                // The dots are replaced by Namespace separator
                // The parts of the segment are Capitalized
                $class = implode('\\', array_map(function ($seg) {
                    return ucfirst($seg);
                }, explode('.', $class)));

                return sprintf('Ahc\Controller\%sController::%sAction', $class, $method);
            }
        );

        //
        $app['json.add_meta'] = $app->protect(
            function (array $meta) use ($app) {
                if (!isset($app['json.meta'])) {
                    $app['json.meta'] = [];
                }
                $meta = array_merge($app['json.meta'], $meta);
                $app['json.meta'] = $meta;
            }
        );

        // Register settings repo
        $app['settings_repo'] = $app->share(
            function ($app) {
                return new SettingRepository($app);
            }
        );

        // Register tweets repo
        $app['tweets_repo'] = $app->share(
            function ($app) {
                return new TweetRepository($app);
            }
        );

        // Register geo repo
        $app['geo_repo'] = $app->share(
            function ($app) {
                return new GeoRepository($app);
            }
        );

        // Register histories repo
        $app['histories_repo'] = $app->share(
            function ($app) {
                return new HistoryRepository($app);
            }
        );
    }
}
