<?php

namespace Ahc\Provider;

use Silex\Application;
use Silex\ServiceProviderInterface;
use TwitterAPIExchange;

/**
 * Twitter service provider wrapped around TwitterAPIExchange.
 *
 * @author Jitendra Adhikari <jiten.adhikary@gmail.com>
 */
class TwitterServiceProvider implements ServiceProviderInterface
{
    /**
     * {@inheritdoc}
     */
    public function boot(Application $app)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function register(Application $app)
    {
        $this->setDefaults($app);

        // One time initiate twitter api handler
        $app['twitter.handler'] = $app->share(function ($app) {
            return new TwitterAPIExchange($app['twitter.credentials']);
        });

        // The tweets search helper
        $app['twitter.search_tweets'] = $app->protect(
            function ($q, $geocode = '', $count = 15, array $options = []) use ($app) {
                if (is_array($q)) {
                    $q = implode('+OR+', $q);
                }
                $validKeys = array_fill_keys(
                    explode(' ', 'lang locale result_type until since_id max_id include_entities'),
                    1
                );
                $options = array_intersect_key($options, $validKeys);
                $query = sprintf('?q=%s&geocode=%s&count=%d', $q, $geocode, $count);
                foreach ($options as $key => $value) {
                    if (is_bool($value)) {
                        $value = $value ? 'true' : 'false';
                    }
                    $query .= sprintf('&%s=%s', $key, $value);
                }

                return $app['twitter.handler']
                    ->setGetfield($query)
                    ->buildOauth(
                        $app['twitter.tweet_search_endpoint'],
                        $app['twitter.request_method']
                    )
                    ->performRequest();
            }
        );
    }

    /**
     * Set default parameters required by migration if only
     * they are not already injected to DI wrapper (the app).
     *
     * @param Application $app
     */
    protected function setDefaults(Application $app)
    {
        $defaults = [
            'oauth_access_token' => '962030066-H3ue1gp34LxQHFK7JQh7RiOqlgBhmE0cAScno5XT',
            'oauth_access_token_secret' => '9OoYHD2okfwow2ul8Jy0bOFDMbM0d61iIM7idXCUC5VnS',
            'consumer_key' => '7juzoXJpb84lSNm1DSYbVDn3x',
            'consumer_secret' => 'zwI25TNYG4icXCpB3cKrgi8hoc74GamkalOe1QhfZl6UdJOrh8',
        ];
        if (!isset($app['twitter.credentials'])) {
            $app['twitter.credentials'] = $defaults;
        }
    }
}
