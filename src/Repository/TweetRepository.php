<?php

namespace Ahc\Repository;

/*
 * Provides easy interfaces to access tweets either
 * from cache or consumer api as well as cache them.
 */
class TweetRepository extends AbstractRepository
{
    /**
     * Lookup for tweets with valid cache lifetime
     * or consume from api and rebuilds the cache.
     *
     * @param  string  The location to match
     * @param  string  The latitude longitude delimited by comma
     *
     * @return mixed An array of tweets matching the location data with valid cache lifetime
     *               A string denoting error message incase of failure
     */
    public function lookup($location, $latlong)
    {
        if (empty($location) or empty($latlong)) {
            return 'Insufficient parameters given';
        }

        $cachettl = $this->app['settings_repo']->get('twitter.cache_ttl_min', 60);
        $cachettl *= 60000; // Minute to milliseconds
        $timelimit = $this->getMilliSeconds() - $cachettl;

        // First check cache if there are tweets for this location
        $tweets = $this->db->fetchAssoc(
            'select `id`, `tweets`, `searched_on` from `tweets` where `location` = :location',
            compact('location')
        );
        $cacheId = null;
        if ($tweets) {
            $cacheId = $tweets['id'];
            // Are these still within the cache ttl limit?
            if ((float) $tweets['searched_on'] > $timelimit) {
                $this->addJsonMeta([
                    'is_cache' => true,
                    'rsrc_id' => $cacheId,
                    'searched_on' => (float) $tweets['searched_on'],
                    'cachettl' => $cachettl,
                ]);

                return json_decode($tweets['tweets'], true) ?:
                    'No tweets found for this location: '.$location;
            }
        }

        $this->addJsonMeta([
            'is_cache' => false
        ]);

        // Fetch new tweets
        $tweets = $this->fetchTweets($location, $latlong);
        // Then store in cache
        $this->cacheTweets($tweets, $location, $latlong, $cacheId);

        return $tweets;
    }

    /**
     * Reloads paginated tweets for a given location and rebuild the cache
     *
     * @param  numeric $id    The resource identifier to reload
     * @param  numeric $maxId The pagination identifier
     * @return array          The new tweets for the page as available per $maxId
     */
    public function reload($id, $maxId)
    {
        if (empty($id) or empty($maxId)) {
            return 'Insufficient parameters given';
        }

        $tweets = $this->db->fetchAssoc(
            'select * from `tweets` where `id` = :id',
            compact('id')
        );
        if (!$tweets) {
            return 'Tweets not found for given id: '.$id;
        }
        $location = $tweets['location'];
        $latlong = $tweets['latitude'].','.$tweets['longitude'];

        // Fetch new tweets
        $newTweets = $this->fetchTweets($location, $latlong, $maxId);
        // Validate
        if (is_array($newTweets)) {
            $tweets = json_decode($tweets['tweets'], true);
            $tweets = array_merge($tweets, $newTweets);
            $this->cacheTweets($tweets, $location, $latlong, $id);
        }

        return $newTweets;
    }

    /**
     * @param  string  The location to match
     * @param  string  The latitude longitude delimited by comma.
     *
     * @return mixed   Array of tweets if available, error string otherwise
     */
    public function fetchTweets($location, $latlong, $maxId = 0)
    {
        $count = $this->app['twitter.results_per_page'];
        $radius = $this->app['settings_repo']
            ->get('twitter.search_radius_km', 50);
        $geocode = $latlong.','.$radius.'km';
        $options = [
            'include_entities' => false,
            'result_type' => 'recent',
        ];
        if ($maxId) {
            $options['max_id'] = $maxId;
        }

        try {
            $payload = $this->app['twitter.search_tweets']($location, $geocode, $count, $options);
        } catch (\Exception $e) {
            return $e->getMessage();
        }

        $payload = json_decode($payload, true);
        if (empty($payload['statuses'])) {
            return 'No tweets found for this location: '.$location;
        }

        $tweets = [];
        // Normalize, wipe out extraneous data!
        foreach ($payload['statuses'] as $status) {
            // Skip the ones without geocodes
            if (empty($status['geo']['coordinates'])) {
                continue;
            }
            $tweet['tweet'] = $status['text'];
            $tweet['when'] = date('Y-m-d H:i:s', strtotime($status['created_at']));
            $tweet['avatar'] = $status['user']['profile_image_url'];
            $tweet['geo'] = $status['geo']['coordinates'];

            array_push($tweets, $tweet);
        }

        if (isset($payload['search_metadata']['next_results'])) {
            parse_str(trim($payload['search_metadata']['next_results'], '?'), $maxId);
            $this->addJsonMeta([
                'max_id' => $maxId['max_id'],
            ]);
        }

        if (empty($tweets)) {
            return 'No tweets found for this location '.$location;
        }

        return $tweets;
    }

    /**
     * @param  array   The fresh tweets to be cached
     * @param  string  The location
     * @param  string  The latitude longitude delimited by comma.
     *
     * @return bool True if tweets caching success, false otherwise
     */
    public function cacheTweets($tweets, $location, $latlong, $id = null)
    {
        if (empty($tweets) or is_string($tweets)) {
            return false;
        }
        $tweets = json_encode($tweets);
        $searched_on = $this->getMilliSeconds();
        try {
            if ($id) {
                // Update existing (Note: db->update fails)
                $this->db->executeQuery('update `tweets` set `tweets`= ?, `searched_on` = ? where `id` = ?',
                    [$tweets, $searched_on, $id]
                );
            } else {
                // Insert new
                list($latitude, $longitude) = explode(',', $latlong);
                $id = $this->insert(compact('location', 'latitude', 'longitude', 'tweets', 'searched_on'), compact('id'));
            }
            $cachettl = $this->app['settings_repo']
                    ->get('twitter.cache_ttl_min', 60);
            $cachettl *= 60000; // Minutes to milliseconds
            $this->addJsonMeta([
                'rsrc_id' => $id,
                'searched_on' => (float) $searched_on,
                'cachettl' => $cachettl,
            ]);

            return true;
        } catch (\Exception $e) {
        }

        return false;
    }

    /**
     * {@inheritdoc}
     */
    protected function getTable()
    {
        return 'tweets';
    }

    /**
     * {@inheritdoc}
     */
    protected function getColumns()
    {
        return ['location', 'latitude', 'longitude', 'tweets', 'searched_on'];
    }

    protected function getMilliSeconds()
    {
        return round(1000 * microtime(true));
    }
}
