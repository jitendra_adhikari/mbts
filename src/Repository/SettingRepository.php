<?php

namespace Ahc\Repository;

/**
 * Setting Repository
 */
class SettingRepository extends AbstractRepository
{
    protected $cache = null;

    public function init()
    {
        if (null !== $this->cache) {
            return;
        }
        $this->cache = [];
        foreach ($this->all() as $set) {
            $this->cache[$set['name']] = $set['value'];
        }
    }

    public function get($name = null, $default = null)
    {
        if (null === $name) {
            return $this->cache;
        }

        return isset($this->cache[$name]) ? $this->cache[$name] : $default;
    }

    /**
     * Sets settings (permanently saves to DB).
     *
     * @param string $name  The key name
     * @param string $value The key value
     *
     * @return bool         True if success, false otherwise
     */
    public function set($name, $value)
    {
        // For existing setting name, update if only the value is changed
        if (array_key_exists($name, $this->cache)) {
            if ($this->cache[$name] !== $value) {
                $bool = (bool) $this->update(compact('value'), compact('name'));
                $this->cache[$name] = $value;

                return $bool;
            }

            return false;
        }

        // Insert a new entry
        $bool = (bool) $this->insert(compact('name', 'value'));
        $this->cache[$name] = $value;

        return $bool;
    }

    /**
     * {@inheritdoc}
     */
    protected function getTable()
    {
        return 'settings';
    }

    /**
     * {@inheritdoc}
     */
    protected function getColumns()
    {
        return ['name', 'value'];
    }
}
