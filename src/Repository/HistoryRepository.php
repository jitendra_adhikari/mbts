<?php

namespace Ahc\Repository;

/**
 * History Repository
 */
class HistoryRepository extends AbstractRepository
{
    public function save($location, $cookie)
    {
        $this->addJsonMeta([
            'is_new' => false,
        ]);

        if (empty($location) or empty($cookie)) {
            return 'Insufficient parameters given';
        }

        $history = $this->findOneBy(compact('location', 'cookie'));
        if ($history) {
            $id = $history['id'];
            $location = $history['location'];

            return compact('id', 'location');
        }

        $this->addJsonMeta([
            'is_new' => true,
        ]);

        $id = $this->insert(compact('location', 'cookie'));

        return compact('id', 'location');
    }

    public function history($cookie)
    {
        $this->addJsonMeta([
            'count' => 0,
        ]);

        if (empty($cookie)) {
            return 'Invalid data';
        }

        $histories = $this->db->fetchAll('select `id`, `location` from `histories` where `cookie` = :cookie order by `id` asc', compact('cookie')) ?: [];

        $this->addJsonMeta([
            'count' => count($histories),
        ]);

        return $histories;
    }

    /**
     * {@inheritdoc}
     */
    protected function getTable()
    {
        return 'histories';
    }

    /**
     * {@inheritdoc}
     */
    protected function getColumns()
    {
        return ['cookie', 'location'];
    }
}
