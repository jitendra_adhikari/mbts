<?php

namespace Ahc\Repository;

/**
 * Geo Repository
 */
class GeoRepository extends AbstractRepository
{
    /**
     * Lookup geocode (coordinates) for given location
     *
     *
     * @param  [type] $location [description]
     * @return [type]           [description]
     */
    public function lookup($location)
    {
        if (empty($location)) {
            return 'Insufficient parameters given';
        }
        // First search if we aleady have it in DB
        $coords = $this->db->fetchAssoc('select `latitude`, `longitude` from `tweets` where `location` = :location', compact('location'));

        // Then, Delegate to the Geolocation provider
        if (!$coords) {
            try {
                $coords = $this->app['geo.get_coords']($location);
            } catch (\Exception $e) {
                return 'Error geocoding: '.$e->getMessage();
            }
        }

        // Validate
        if (empty($coords['latitude']) or empty($coords['longitude'])) {
            return 'Geocoding failed for given location: '.$location;
        }

        return $coords;
    }

    /**
     * {@inheritdoc}
     */
    protected function getTable()
    {
        return 'tweets';
    }

    /**
     * {@inheritdoc}
     */
    protected function getColumns()
    {
        return ['location', 'latitude', 'longitude', 'tweets', 'searched_on'];
    }
}
