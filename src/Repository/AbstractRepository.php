<?php

namespace Ahc\Repository;

use Silex\Application;

/**
 * A base class for all repositories responsible for DB CRUD ops.
 */
abstract class AbstractRepository
{
    /**
     * @var \Doctrine\DBAL\Connection
     */
    protected $db;

    /**
     * @var \Silex\Application
     */
    protected $app;

    /**
     * Metadata collection. Array of mixed values.
     *
     * @var array
     */
    protected $metadata = [];

    /**
     * Errors collection. Array of error strings.
     *
     * @var array
     */
    protected $errors = [];

    public function __construct(Application $app)
    {
        $this->app = $app;
        $this->db = $app['db'];
        $this->init();
    }

    /**
     * Get the total count of rows in a table.
     *
     * @return int
     */
    public function total()
    {
        $table = $this->getTable();

        return $this->db->fetchColumn('SELECT COUNT(*) FROM '.$table);
    }

    /**
     * Get all the datas in a table.
     *
     * @return mixed
     */
    public function all()
    {
        $table = $this->getTable();

        return $this->db->fetchAll('select * from '.$table);
    }

    /**
     * @return mixed The last inserted row identifier or null
     */
    public function insert(array $data)
    {
        $data = $this->validate($data);
        if (empty($data)) {
            return;
        }
        $this->db->insert($this->getTable(), $data);

        return $this->db->lastInsertId();
    }

    /**
     * @return int The number of affected rows.
     */
    public function update(array $data, array $where)
    {
        $data = $this->validate($data);
        $where = $this->validate($where);

        if (empty($data) or empty($where)) {
            return 0;
        }

        return $this->db->update(
            $this->getTable(),
            $data,
            $where
        );
    }

    /**
     * @return int The number of affected rows.
     */
    public function delete(array $where)
    {
        $where = $this->validate($where);
        if (empty($where)) {
            return 0;
        }

        return $this->db->delete(
            $this->getTable(),
            $where
        );
    }

    /**
     * Find a collection of rows matching the given where conditions.
     *
     * @param array $where The filters to be used in query where clause
     *
     * @return array An array of multiple entries of data
     */
    public function findBy(array $where)
    {
        $query = $this->buildQuery($where);

        return $this->db->fetchAll($query, $where);
    }

    /**
     * Find a row matching the given where conditions.
     *
     * @param array $where The filters to be used in query where clause
     *
     * @return array An assoc array of single entry of data
     */
    public function findOneBy(array $where)
    {
        $query = $this->buildQuery($where);

        return $this->db->fetchAssoc($query, $where);
    }

    /**
     * Simulates __construct() for subclasses.
     */
    public function init()
    {
    }

    /**
     * Validates the input data, by filtering out unknown keys.
     *
     * @param  array The input data
     *
     * @return array The validated data
     */
    protected function validate(array $data)
    {
        $columns = $this->getColumns();
        $columns = array_fill_keys($columns, 1);

        return array_intersect_key($data, $columns);
    }

    /**
     * Adds metadata to app instance.
     *
     * @param array $meta [description]
     */
    protected function addJsonMeta(array $meta)
    {
        $this->app['json.add_meta']($meta);
    }

    /**
     * Adds error string.
     *
     * @param string $error The error description
     */
    protected function addError($error)
    {
        $this->errors[] = (string) $error;
    }

    /**
     * Checks if there is atleast one error in the stack.
     *
     * @return bool
     */
    public function hasError()
    {
        return count($this->errors) > 0;
    }

    /**
     * Builds a select query by using given filters.
     *
     * @param array &$where The filters to be used as where clause
     *
     * @return string The compiled query string
     */
    protected function buildQuery(array &$where)
    {
        foreach ($where as $colm => &$value) {
            if (is_numeric($colm)) {
                $query[] = $value;
                unset($where[$colm]);
            } elseif (is_null($value)) {
                $query[] = sprintf('`%s` is null', $colm);
                unset($where[$colm]);
            } elseif (is_bool($value)) {
                $value = (int) $value;
                $query[] = sprintf('`%s` = :%s', $colm, $colm);
            } elseif (is_array($value)) {
                $value = array_map(function ($v) {
                    is_numeric($v) ? $v : "'$v'";
                }, $value);
                $value = implode(', ', $value);
                $query[] = sprintf('`%s` in (:%s)', $colm, $colm);
            } else {
                $value = (string) $value;
                $query[] = sprintf('`%s` = :%s', $colm, $colm);
            }
        }

        return sprintf('select * from %s where ', $this->getTable())
               .implode(' and ', $query);
    }

    /**
     * Allow the sub class to provide their table names.
     *
     * @return string The table name
     */
    abstract protected function getTable();

    /**
     * Allow the sub class to provide name of columns their table holds.
     *
     * @return array The column names in table
     */
    abstract protected function getColumns();
}
