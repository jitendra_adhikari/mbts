<?php

namespace Ahc\Controller;

use Silex\Application;
use Silex\ControllerCollection;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Controller provider for '/api/*' family of routes.
 */
class ApiController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function route(ControllerCollection $ctrl)
    {
        // Use GET method for simple read requests
        $ctrl->get('/echo', [$this, 'echoAction'])
            ->bind('echo');
        $ctrl->get('/geo-lookup', [$this, 'geoSearchAction'])
            ->bind('geo_lookup');
        $ctrl->get('/get-history', [$this, 'historyGetAction'])
            ->bind('get_history');

        // Use POST method for write operations
        $ctrl->post('/tweets-lookup', [$this, 'tweetSearchAction'])
            ->bind('tweets_lookup');
        $ctrl->post('/tweets-update', [$this, 'tweetUpdateAction'])
            ->bind('tweets_update');
        $ctrl->post('/save-history', [$this, 'historySaveAction'])
            ->bind('save_history');
    }

    /**
     * Echoes back anything sent in 'what' param
     * The echoing is either text or json (default).
     *
     * @param Request $request  Auto injected.
     *
     * @return mixed
     */
    public function echoAction(Request $request)
    {
        $what = $request->query->get('what');
        $how = $request->query->get('how', 'json');

        if ('text' === $how) {
            return $what;
        }

        return new JsonResponse(compact('what'));
    }

    /**
     * Responds to GET /api/geo-lookup.
     *
     * @param Request $request The Request instance is automatically injected
     */
    public function geoSearchAction(Request $request)
    {
        $location = $request->query->get('location');

        return $this->json($this->get('geo_repo')->lookup($location));
    }

    /**
     * Responds to POST /api/tweets-lookup.
     *
     * @param Request $request The Request instance is automatically injected
     */
    public function tweetSearchAction(Request $request)
    {
        $location = $request->request->get('location');
        $latlong = $request->request->get('latlong');

        return $this->json($this->get('tweets_repo')->lookup($location, $latlong));
    }

    /**
     * Responds to POST /api/tweets-update.
     *
     * @param Request $request The Request instance is automatically injected
     */
    public function tweetUpdateAction(Request $request)
    {
        $rsrcId = $request->request->get('rsrc_id');
        $maxId = $request->request->get('max_id');

        return $this->json($this->get('tweets_repo')->reload($rsrcId, $maxId));
    }

    /**
     * Responds to POST /api/save-history.
     *
     * @param Request $request The Request instance is automatically injected
     *
     * @return JsonResponse    The JsonResponse object
     */
    public function historySaveAction(Request $request, Application $app)
    {
        $identifier = $this->get('session.cookie_id');
        $location = $request->request->get('location');

        return $this->json($this->get('histories_repo')
            ->save($location, $identifier)
        );
    }

    /**
     * Responds to GET /api/get-history.
     *
     * @param Request $request The Request instance is automatically injected
     *
     * @return JsonResponse    The JsonResponse object
     */
    public function historyGetAction(Request $request)
    {
        $identifier = $this->get('session.cookie_id');

        return $this->json($this->get('histories_repo')
            ->history($identifier)
        );
    }
}
