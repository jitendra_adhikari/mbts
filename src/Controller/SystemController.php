<?php

namespace Ahc\Controller;

use Silex\ControllerCollection;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Controller provider for '/system/*' family of routes.
 */
class SystemController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function route(ControllerCollection $ctrl)
    {
        $ctrl->match('/migrate', [$this, 'migrateAction']);
        $ctrl->match('/purge', [$this, 'purgeAction']);
        $ctrl->match('/settings', [$this, 'settingsAction'])
            ->bind('settings');
    }

    /**
     * Responds to /system/settings/, it is an endpoint to
     * configure the options for tweet search api.
     *
     * @param Request $request
     *
     * @return Response|RedirectResponse
     */
    public function settingsAction(Request $request)
    {
        $repo = $this->get('settings_repo');
        $data = [
            'radius' => $repo->get('twitter.search_radius_km', 50),
            'cachettl' => $repo->get('twitter.cache_ttl_min', 60),
        ];

        if ($request->isMethod('POST')) {
            $repo->set(
                'twitter.search_radius_km',
                $request->request->get('twitter[search_radius_km]', $data['radius'], true)
            );
            $repo->set(
                'twitter.cache_ttl_min',
                $request->request->get('twitter[cache_ttl_min]', $data['cachettl'], true)
            );

            return new RedirectResponse($this->get('url_generator')->generate('settings'));
        }

        return $this->get('twig')->render('system/settings.html', $data);
    }

    /**
     * Intended for purging database in development. Not used anymore.
     *
     * @param Request $request
     *
     * @return string|Response
     */
    public function purgeAction(Request $request)
    {
        return 'Not implemented';
    }

    /**
     * Intended for migrating database schema in development.
     * Not used anymore, the functionality has been migrated to cli command.
     *
     * @param Request $request
     *
     * @return string|Response
     */
    public function migrateAction(Request $request)
    {
        return 'Not implemented';
    }
}
