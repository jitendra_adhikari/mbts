<?php

namespace Ahc\Controller;

use Silex\Application as SilexApp;
use Silex\ControllerCollection;
use Silex\ControllerProviderInterface as Contract;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * A base class for all controllers with a mountpoint
 * For example: '/system/*' or '/api/*' family of controllers.
 */
abstract class BaseController implements Contract
{
    /**
     * @var Silex\Application The application instance
     */
    protected static $app;

    /**
     * Hooks into the Controller provider's connect() method to allow settng up routes.
     */
    abstract public function route(ControllerCollection $ctrl);

    /**
     * {@inheritdoc}
     */
    public function connect(SilexApp $app)
    {
        // Save the instance
        self::$app = $app;

        $ctrl = $app['controllers_factory'];

        // Allow the subclasses to set the routes they are interested in
        $this->route($ctrl);

        return $ctrl;
    }

    /**
     * Get the service (or anything) via dependency injection container.
     *
     * @param  string The service name that has been injected to App instance
     *
     * @return mixed
     */
    public function get($service)
    {
        return self::$app[$service];
    }

    /**
     * A handy wrapper to create JsonResponse.
     *
     * @param mixed $data    It can be a simple error string or complex structure
     *
     * @return JsonResponse
     */
    public function json($data)
    {
        // Stringy data is just an error message!
        $status = false === is_string($data);
        if ($status) {
            return new JsonResponse(compact('status', 'data'));
        }

        $message = $data;

        return new JsonResponse(compact('status', 'message'));
    }

    /**
     * Data validator. Not yet used.
     *
     * @param  array
     * @param  array
     * @param  string
     */
    public function validate($data, $rules, $tmpl = '{path} {error}')
    {
        $errors = $this->get('validator')->validateValue($data, $rules);
        if (count($errors) > 0) {
            foreach ($errors as $error) {
                // $error->getPropertyPath().' '.$error->getMessage();
            }
        }
    }
}
