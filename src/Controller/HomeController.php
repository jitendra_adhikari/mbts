<?php

namespace Ahc\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Response;

/**
 * Home Controller handles request to '/' route.
 */
class HomeController
{
    /**
     * @param  Application  The app instance is auto injected
     *
     * @return string       The view response rendered by twig
     */
    public function indexAction(Application $app)
    {
        return $app['twig']->render('home/index.html', array());
    }

    /**
     * @param  Application  The app instance is auto injected
     *
     * @return string       The view response rendered by twig
     */
    public function testAction(Application $app)
    {
        return $app['twig']->render('home/test.html', array());
    }

    /**
     * @param  Application  The app instance is auto injected
     *
     * @return Response     The view response rendered by twig
     */
    public function coreJsAction(Application $app)
    {
        $response = new Response($app['twig']->render('home/core.js.html', array()));
        $response->headers->set('Content-Type', 'text/javascript');

        return $response;
    }
}
