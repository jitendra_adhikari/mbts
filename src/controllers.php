<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

//Request::setTrustedProxies(array('127.0.0.1'));

$app->error(function (\Exception $e, $code) use ($app) {
    if ($app['debug']) {
        return;
    }

    // 404.html, or 40x.html, or 4xx.html, or error.html
    $templates = array(
        'errors/'.$code.'.html',
        'errors/'.substr($code, 0, 2).'x.html',
        'errors/'.substr($code, 0, 1).'xx.html',
        'errors/default.html',
    );

    return new Response($app['twig']->resolveTemplate($templates)->render(array('code' => $code)), $code);
});

$app->get('/', $app['utils.controller']('home'))
    ->bind('home');
$app->get('/core.js', $app['utils.controller']('home::coreJs'))
    ->bind('core_js');
$app->get('/test', $app['utils.controller']('home::test'))
    ->bind('test');

$app->mount('/api', new Ahc\Controller\ApiController());
$app->mount('/system', new Ahc\Controller\SystemController());

$app->get('/cookie', function(Request $request) use ($app) {
    $app['json.meta'] = ['some', 'thing'];

    return $app->json(['lol' => 'LOL']);
});
