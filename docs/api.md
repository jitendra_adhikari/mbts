# mbts api
- restful
- json based

# architecture
- responses are architecured like so
```json
	{
		status: true,
		data: [
			{ row1: { col1: "val1", col2: "val3" } },
			{ row2: { col1: "val2", col2: "val4" } },
		],
		meta: {
			key1: "value1",
			key2: "value2",
		}
	}
```
- `status` signifies success or failure
- `data` may be singleton (object) or collection (array or objects)
- `meta` is the metadata with `key`:`value` pair

# endpoints
the api enpdoint uris are namespaced `/api/`
below endpoints are available

## get histories
- resource: GET /api/get-history
- type: collection
- example response:
```json
	{
		status: true,
		data: [
			{ id: 1, location: "Damauli" }
		]
	}
```

## save history
- resource: POST /api/save-history
- type: singleton
- params: `location` : string
- example response:
```json
	{
		status: true,
		data: {
			id: 1,
			location: "Damauli"
		}
	}
```

## geo search
- resource: GET /api/geo-lookup
- type: singleton
- params: `location` : string
- example response:
```json
	{
		status: true,
		data: {
			latitude: 12.34214,
			longitude: 100.33242
		}
	}
```

## tweets search
- resource: POST /api/tweets-lookup
- params: `location` : string, `latlong` : csv string (latitude,longitude)
- example response:
```json
	{
		status: true,
		data: [
			{ tweet: "tweet text", avatar: "image url", geo: [12.323, 88.123], when: "Y-m-d H:i:s" }
		]
	}
```
## tweets update
- resource: POST /api/tweets-update
- params: `rsrc_id` : number, `max_id` : number
- example response: same as tweets search
