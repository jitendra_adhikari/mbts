# requirements

- `php` 5.4 or above
- `curl` extension
- `pdo_sqlite` extension
- `composer`
- `phpunit` to run unit tests
- bitbucket account with ssh-keys managed


# steps

- clone the repo: `git clone git@bitbucket.org:jitendra_adhikari/mbts.git`
- install deps: `cd mbts && composer install -o --prefer-dist`
- `var/` path has to be writable (change file ownership &/or permission as seen fit, below commands are only for example:)
	- `chown -R user:group var/`
	- `chmod -R 0755 var/`
- database schema migration:
	- check: `bin/console migrations:status`
	- migrate: `bin/console migrations:migrate`
- now you should be able to launch the app via web browser
- see `usage.md` for more!
