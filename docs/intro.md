# mbts

**mbts** is a map based tweet search web application built on top of silex micro framework using google maps and twitter apis. it uses sqlite via pdo as database.

# deps

besides standard dependencies (mostly symfony components) of silex, the following extra packages are used:

- `doctrine/migrations` : for database schema migration
- `j7mbo/twitter-api-php` : for searching tweets using twitter restful api
- `jeroendesloovere/geolocation-php-api` : for geocoding input locations
