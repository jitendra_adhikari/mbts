# getting up

## development mode
- `cd /path/to/mbts/`
- `php -S localhost:8000 -t web/`
- browse to [localhost:8000](http://localhost:8000/index_dev.php)

## production
- add the path `/web` to document root
- browse to the fully qualified domain name


# unit tests

## backend
- `cd /path/to/mbts/`
- `phpunit`

## frontend
- browse to qunit endpoint [test](http://localhost:8000/index_dev.php/test)


# system setting

the tweet search radius and cache storage timeout are configurable.

- browse to [settings](http://localhost:8000/index_dev.php/system/settings)
