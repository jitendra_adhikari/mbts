<?php

// Set default timezone to UTC
ini_set('date.timezone', 'UTC');
// Donot display errors in PROD environment
ini_set('display_errors', 'Off');

require_once __DIR__.'/../vendor/autoload.php';

$app = require __DIR__.'/../src/app.php';
require __DIR__.'/../config/prod.php';
require __DIR__.'/../src/controllers.php';
$app->run();
