var Mockjax = {
  getHistory: {
    status: true,
    data: [
      {id: 0, location: '_empty'}
    ],
    meta: {
      count: 1
    }
  },
  saveHistory: {
    status: true,
    data: {id: 2, location: 'Pokhara'},
    meta: {
      is_new: true
    },
    param: {
      location: 'Pokhara'
    }
  },
  lookupGeo: {
    status: true,
    data: {
      latitude: 28.237987,
      longitude: 83.9955879
    },
    meta: {
    },
    param: {
      location: 'Pokhara'
    }
  },
  lookupTweets: {
    status: true,
    data: [
      {
        tweet: "Sunrise at pokhara's lake Nepal.\n#nepal #pokhara #nature #photooftheday #trekking #landscape @\u2026 https:\/\/t.co\/x92Bp1kSjm",
        when: "2015-11-15 02:24:36",
        avatar: "http:\/\/pbs.twimg.com\/profile_images\/572637232986644481\/vrztOpFG_normal.jpeg",
        geo: [28.20659425,83.96143887]
      },
      {
        tweet: "Thanks to its access to world-class climbing, whitewater, mountain biking, and paragliding, Pokhara,\u2026 https:\/\/t.co\/LatFGz1Ixs",
        when: "2015-11-14 16:49:56",
        avatar: "http:\/\/pbs.twimg.com\/profile_images\/537718436776771584\/o1aAmnUT_normal.jpeg",
        geo: [28.2149056,83.9581575]
      }
    ],
    meta: {
      rsrc_id: 1,
      is_cache: true,
      searched_on: +new Date(),
      cachettl: 60*60000
    },
    param: {
      location: 'Pokhara',
      latlong: {
        latitude: 28.237987,
        longitude: 83.9955879
      }
    }
  },
  updateTweets: {

  }
}

jQuery.mockjaxSettings.responseTime = 0;
jQuery.mockjaxSettings.contentType = 'application/json';

// Ajax mocks
jQuery.mockjax({
  url: /\/api\/get\-history/,
  type: 'get',
  responseText: Mockjax.getHistory
});

jQuery.mockjax({
  url: /\/api\/save\-history/,
  type: 'post',
  responseText: Mockjax.saveHistory
});

jQuery.mockjax({
  url: /\/api\/geo\-lookup/,
  type: 'get',
  responseText: Mockjax.lookupGeo
});

jQuery.mockjax({
  url: /\/api\/tweets\-lookup/,
  type: 'post',
  responseText: Mockjax.lookupTweets
});
