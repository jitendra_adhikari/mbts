// Unit tests for APP module
QUnit.module('APP', function(hooks) {
  hooks.beforeEach(function() {
    this.app = new APP();
    this.dgi = function(id) {
      return document.getElementById(id);
    };
  });

  QUnit.test('app.configure', function(assert) {
    this.app.configure();
    assert.equal(this.app.config.zoom, 10);
  });

  QUnit.test('app.registerEvents', function(assert) {
    this.app.registerEvents();

    assert.ok(gmap.event.hasListeners(window, 'load'));
    assert.ok(gmap.event.hasListeners(window, 'resize'));
    assert.ok(jQuery._data(this.dgi('search-button'), 'events'));
  });

  QUnit.test('app.getHistory', function(assert) {
    var
      expected = Mockjax.getHistory.data,
      done = assert.async(),
      app = this.app
    ;
    app.getHistory();

    setTimeout(function() {
      jQuery.each(expected, function(i, exp) {
        assert.equal(app.cache.histories[exp.location], exp.id);
      });
      done();
    }, 100);

    assert.expect(expected.length);
  });

  QUnit.test('app.saveHistory', function(assert) {
    var
      location = Mockjax.saveHistory.param.location.toLowerCase(),
      expected = Mockjax.saveHistory.data,
      done = assert.async(),
      app = this.app
    ;
    app.saveHistory(location);

    setTimeout(function() {
      // The recent location should be in app.cache
      assert.ok(app.cache.histories[location]);
      done();
    }, 100);

    assert.expect(1);
  });

  QUnit.test('app.lookupGeo', function(assert) {
    var
      location = Mockjax.lookupGeo.param.location.toLowerCase(),
      expected = Mockjax.lookupGeo.data,
      done = assert.async(),
      app = this.app
    ;
    app.configure();
    app.lookupGeo(location);

    setTimeout(function() {
      assert.ok(app.cache.locations[location].latitude);
      assert.ok(app.cache.locations[location].longitude);
      done();
    }, 100);
  });

  QUnit.test('app.lookupTweets', function(assert) {
    var
      location = Mockjax.lookupTweets.param.location.toLowerCase(),
      latlong = Mockjax.lookupTweets.param.latlong,
      expected = Mockjax.lookupTweets.data,
      done = assert.async(),
      app = this.app
    ;
    app.configure();
    app.lookupTweets(location, latlong);
    setTimeout(function() {
      assert.ok(app.cache.tweets[location].data.length == 2);
      done();
    }, 100);
  });

  QUnit.test('app.markTweets', function(assert) {
    this.app.configure();
    this.app.draw(Mockjax.lookupGeo.data);
    this.app.markTweets(Mockjax.lookupTweets.data);

    assert.expect(0);
  });

  QUnit.test('app.feedback', function(assert) {
    this.app.feedback('Hello, Testing feedback');

    assert.ok(jQuery('#feedback-overlay').is(':visible'));
    assert.equal(jQuery('#feedback-message').text(), 'Hello, Testing feedback');

    // var done = assert.async();
    setTimeout(function() {
      jQuery('#feedback-close-btn').trigger('click');
      assert.ok(jQuery('#feedback-overlay').is(':hidden'));
      // done();
    }, 100);
  });
});
