'use strict';
/**
 * Map application built on top of google maps and jQuery
 *
 * Usage: var app = new APP(debug);
 *            app.run();
 *
 * @author Jitendra Adhikari <jiten.adhikary@gmail.com>
 */
var
// A truthy debug value is useful for debugging the app on development
  APP = function(debug) {
    // Holds cached prefetched data for speed and better UX
    this.cache = {
      "locations": {},
      "histories": {},
      "tweets": {},
    };

    // Configuration for google maps
    this.config = {};

    // Logs the debug info into console, if debug is enabled
    this.debug = !!debug;
  },

  // Local shorthand ref to google.maps
  gmap = google.maps;

/**
 * Draws the map on viewport, if latlong is provided also repositions map center
 *
 * @param  object latlong  The coordinates in format: {'latitude':27.7, 'longitude': 85.3}
 * @return void
 */
APP.prototype.draw = function(latlong) {
  if (latlong) {
    // Reposition the map center
    this.config.center.lat = this.cache.latitude = parseFloat(latlong.latitude);
    this.config.center.lng = this.cache.longitude = parseFloat(latlong.longitude);
  }

  this.map = new gmap.Map(document.getElementById('map-canvas'), this.config);
};

/**
 * Registers application events for both the google maps and user actions
 *
 * @return void
 */
APP.prototype.registerEvents = function() {
  var
    self = this,
    // Build a callback for google map events
    // which essentially (re)draws map.
    drawMap = function() {
      self.draw.call(self);
      // Also, reposition markers if possible
      var location = self.cache.location,
        cache = self.cache.tweets[location]
      ;
      if (cache && cache.data) {
        self.markTweets.call(self, cache.data);
      }
    },

    // The handler for search
    doSearch = function() {
      var location = jQuery.trim(jQuery('#location-input').val());
      if (!location) {
        // self.feedback.call(self, 'Please enter location name');

        return jQuery('#location-input').focus();
      }

      self.lookupGeo.call(self, location);
    },

    // The handler for history visibility toggle
    toggleHistory = function() {
      var el = jQuery('#history');
      if (el.is(':visible')) {
        el.fadeOut('slow');
        return;
      }

      el.fadeIn();
    };

  // Draw the map once window is loaded
  gmap.event.addDomListener(window, 'load', drawMap);

  // For resposiveness, redraw on window resize
  gmap.event.addDomListener(window, 'resize', drawMap);

  // Handle when search button is clicked
  jQuery('#search-button').click(doSearch);
  jQuery('#location-input').keydown(function(event) {
    var keycode = event.keyCode || event.which;
    if (keycode == '13') {
      self.dump('searching on ENTER key press');
      doSearch.call();
    }
  });

  // Handle back and history buttom click
  jQuery('#back-button, #history-button').on('click touchstart', toggleHistory);

  // Handle history item list touch/click
  jQuery('#history-list').on('click touchstart', '.history-item:not(:first)', function() {
    toggleHistory.call();
    jQuery('#location-input').val(jQuery(this).data('location'));
    self.dump('Loading from history');
    doSearch.call();
  });

  // Redraw map on orientation change
  jQuery(window).on('orientationchange', function(event) {
    self.dump('Redraw map on orientationchange');
    drawMap.call();
  });

  // Feedback close
  jQuery('#feedback-close-btn').click(function(){
    jQuery('#feedback-overlay').fadeOut('slow');
  });

  // Delete cache and config on window unload
  jQuery(window).unload(function() {
    delete self.cache;
    delete self.config;
  });
};

/**
 * Places tweets on map with user avatar as marker
 * and tweet text and date as info window.
 *
 * @uses   self.makeInfoContent()
 *
 * @param  array tweets   An array collection of tweets
 *         Each tweet in collection must be an object with below format:
 *         {"tweet":"tweet text", "avatar": "user avatar url", "when": "tweet data", "geo": [latitude, longitude]}
 * @return void
 */
APP.prototype.markTweets = function(tweets) {
  var self = this,
    total = tweets.length,
    info = new google.maps.InfoWindow();

  // Loop through the tweets
  for (var i = 0; i < total; i++) {
    // Does this tweet have geo data ?
    if (!tweets[i]['geo']) continue;

    // Initialize marker
    var marker = new gmap.Marker({
      position: new gmap.LatLng(tweets[i]['geo'][0], tweets[i]['geo'][1]),
      map: self.map,
      icon: tweets[i]['avatar']
    });

    // Gives a callback that pops corresponding infowindow when
    // marker is clicked or touched
    var placeMarker = function(marker, i) {
      return function() {
        info.setContent(self.makeInfoContent(tweets[i]));
        info.open(self.map, marker);
      }
    };

    // Handle when marker is clicked
    gmap.event.addListener(marker, 'click', placeMarker(marker, i));

    // Handle when marker is touched
    gmap.event.addListener(marker, 'touchstart', placeMarker(marker, i));
  }
}

/**
 * Configures the map application, with optional params
 * If the params are not provided, reasonable defaults assumed.
 *
 * @param  float latitude  The latitude coordinate
 * @param  float longitude The longitude coordinate
 * @param  string type     The maptype: roadmap, terrain etc
 * @return void
 */
APP.prototype.configure = function(latitude, longitude, type) {
  var
    latitude = latitude || this.cache.latitude || 27.7,
    longitude = longitude || this.cache.longitude || 85.3;
  this.config = {
    zoom: 10,
    center: {
      "lat": latitude,
      "lng": longitude
    },
    MapTypeId: type || gmap.MapTypeId.ROADMAP,
    MapTypeControl: true,
    streetViewControl: true,
    panControl: true,
    scrollwheel: true,
    draggable: true
  };

  // Cache recent values
  this.cache.latitude = latitude;
  this.cache.longitude = longitude;
}

/**
 * Runs the application, internally does the following:
 * - Reposition map center to user location if possible,
 * - Populate history based on current user cookie
 * - Register the app events
 *
 * @uses   self.draw()
 * @uses   self.getHistory()
 * @uses   self.registerEvents()
 *
 * @return void
 */
APP.prototype.run = function() {
  var self = this;
  self.configure.call(self);

  // Set MAP center to current location if possible
  // 1. If we have saved default map coords in localStorage
  if ('localStorage' in window) {
    self.dump('Map center coordinates loaded from localStorage');
    if (+localStorage.latitude) {
      self.cache._lat = self.config.center.lat = parseFloat(localStorage.latitude);
    }
    if (+localStorage.longitude) {
      self.cache._lng = self.config.center.lng = parseFloat(localStorage.longitude);
    }
  }

  // 2. Or if we have geolocation available in navigator
  if (!(self.cache._lat || self.cache._lng) && navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(pos) {
      var coord = pos.coords;
      self.dump('Map center coordinates saved to localStorage');
      // Save to local storage for later usage
      if ('localStorage' in window) {
        localStorage.latitude = coord.latitude;
        localStorage.longitude = coord.longitude;
      }
      self.draw.call(self, coord);
    });
  }

  // Populate history
  self.getHistory.call(self);

  // Register event handlers
  self.registerEvents.call(self);
};

/**
 * Lookup geo coordinates for given location
 * Caches the coordinates once fetched
 *
 * @uses   self.lookupTweets() To fetch the tweets based on geo coordinates
 *
 * @param  string location     The location/city name
 * @return void
 */
APP.prototype.lookupGeo = function(location) {
  var self = this,
    lwrLoc = location.toLowerCase(),
    geo = self.cache.locations[lwrLoc];

  // We have looked it up already this go!
  if (geo) {
    self.dump('Geocode loaded from cache for '+location);
    self.draw.call(self, geo);
    self.lookupTweets.call(self, location, geo);

    return;
  }
  self.cache.location = lwrLoc;

  // Ajax GET json geo data
  var url = Core.geoUrl + '?location=' + location;
  jQuery.getJSON(url, function(geo) {
    if (!geo.status) {
      self.feedback.call(self, geo.message, 'error');
      throw geo.message;
    }

    // Redraw
    self.dump('Geocode loaded from api for '+location);
    self.draw.call(self, geo.data);
    self.lookupTweets.call(self, location, geo.data);

    // Save to cache
    self.cache.locations[lwrLoc] = geo.data;
  });
};

/**
 * Lookup tweets matching the location within the geo coordinates
 *
 * @uses   self.markTweets()  To place tweet markes on the map
 * @uses   self.saveHistory() To post the search history for current user cookie
 *
 * @param  string location    The location/city name
 * @param  object latlong     The coordinates in format: {'latitude':27.7, 'longitude': 85.3}
 *
 * @return void
 */
APP.prototype.lookupTweets = function(location, latlong) {
  var
    self = this,
    lwrLoc = location.toLowerCase(),
    data = {
      "location": location,
      "latlong": [latlong.latitude, latlong.longitude].join(',')
    },
    chainUpdate = function(tweets) {
      if (!tweets.status) {
        self.feedback.call(self, tweets.message, 'error');
        throw tweets.message;
      }
      self.markTweets.call(self, tweets.data);
      if ('undefined' == typeof self.cache.tweets[lwrLoc]) {
        self.cache.tweets[lwrLoc] = {
          data:[],
          meta: {}
        };
      }
      var meta = tweets.meta;
      jQuery.merge(self.cache.tweets[lwrLoc]['data'], tweets.data);
      jQuery.extend(self.cache.tweets[lwrLoc]['meta'], tweets.meta);
      if (meta && meta.rsrc_id && meta.max_id) {
        var _data = {max_id: meta.max_id, rsrc_id: meta.rsrc_id};
        self.dump('Fetching paginated tweets, max_id ' + _data.max_id);
        jQuery.post(Core.tweetsUpdateUrl, _data, function(tweets) {
          chainUpdate(tweets);
        });
      }
    },
    cache = self.cache.tweets[lwrLoc],
    validateCache = function(cache) {
      if (!cache || !cache.data || !cache.meta) {
        return false;
      }

      var milliseconds = new Date().getTime();
      if (+cache.meta['searched_on']+cache.meta['cachettl'] > milliseconds) {
        self.dump('Tweets loaded from cache for ' + location);

        return true;
      }

      // Empty the stale cache (ttl is over)
      delete self.cache.tweets[lwrLoc];

      return false;
    }
  ;
  // Check if we have cached tweets and if the cache ttl is valid
  if (validateCache(cache)) {
    self.markTweets.call(self, cache.data);

    return;
  }

  // #3. Search info
  jQuery('#map-banner').text('Tweets about ' + location).show();

  // Ajax POST the data
  jQuery.post(Core.tweetsUrl, data, function(tweets) {
    self.dump('Tweets loaded from api for '+location);
    chainUpdate(tweets);
  });

  // Save the search history
  self.saveHistory.call(self, location);
};

/**
 * Saves the location history for current user cookie,
 * Marks the action in cache to prevent future requests.
 *
 * @param  string location The location/city name
 * @return void
 */
APP.prototype.saveHistory = function(location) {
  var self = this,
    lwrLoc = location.toLowerCase(),
    history = self.cache.histories[lwrLoc];
  if (history) {
    return;
  }
  var data = {
    "location": location
  };
  // Ajax post the history data
  jQuery.post(Core.historySaveUrl, data, function(history) {
    if (!history.status) {
      self.feedback.call(self, history.message, 'error');
      throw history.message;
    }
    self.dump('History item saved');
    jQuery('#back-button').after(self.makeList.call(self, history.data));
    self.cache.histories[lwrLoc] = history.data.id;
  });
};

/**
 * Gets the history for current user cookie and
 * append to the history list
 *
 * @return void
 */
APP.prototype.getHistory = function() {
  var self = this;
  jQuery.getJSON(Core.historyGetUrl, function(histories) {
    if (!histories.status) {
      self.feedback.call(self, histories.message, 'error');
      throw histories.message;
    }
    histories = histories.data;
    for (var i = 0; i < histories.length; i++) {
      jQuery('#back-button').after(self.makeList.call(self, histories[i]));
      self.cache.histories[histories[i]['location']] = histories[i]['id'];
    }
    self.dump('History items loaded');
  });
};

/**
 * Builds the infowindow content markup
 *
 * @param  object tweet   The tweet object with format: {"tweet":"some text", "when": "date"}
 * @return string         The content markup
 */
APP.prototype.makeInfoContent = function(tweet) {
  return '<div><p>Tweet: ' + tweet['tweet'] + '</p><p>When: ' + tweet['when'];
};

/**
 * Makes history list item markup
 *
 * @param  object history  The object with format: {"location":"name"}
 * @return string          The list markup
 */
APP.prototype.makeList = function(history) {
  return '<li class="history-item" data-location="' + history['location'] + '">' + history['location'] + '</li>';
}

/**
 * Dumps provided data (or app's config and cache if not provided)
 * to the console when debug mode on
 *
 * @return void
 */
APP.prototype.dump = function(args) {
  if (!this.debug || !window.console) return;
  if (args) {
    return console.log(args);
  }
  console.log('config:\n', this.config, '\n\ncache:\n', this.cache);
};

APP.prototype.feedback = function(message, type) {
  if (!message) {
    throw 'Feedback message required';
  }

  var type = type || 'info';
  jQuery('#feedback').toggleClass(type);
  jQuery('#feedback-type').text(type);
  jQuery('#feedback-message').text(message);
  jQuery('#feedback-overlay').fadeIn();

  setTimeout(function() {
    jQuery('#feedback-overlay').fadeOut('slow');
  }, 4000);
};

